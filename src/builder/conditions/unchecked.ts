/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import { ConditionBlock, pgettext, tripetto } from "tripetto";
import { Checkboxes } from "..";
import ICON_UNCHECKED from "../../../assets/unchecked.svg";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:unchecked`,
    version: PACKAGE_VERSION,
    context: Checkboxes,
    icon: ICON_UNCHECKED,
    get label() {
        return pgettext("block:checkboxes", "All checkboxes unchecked");
    },
})
export class UncheckedCondition extends ConditionBlock {}
