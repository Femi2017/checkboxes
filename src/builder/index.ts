/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Collection,
    NodeBlock,
    affects,
    conditions,
    definition,
    editor,
    npgettext,
    pgettext,
    tripetto,
} from "tripetto";
import { Checkbox } from "./checkbox";
import { CheckboxCondition } from "./conditions/checkbox";
import { UncheckedCondition } from "./conditions/unchecked";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_CHECKED from "../../assets/checked.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:checkboxes", "Checkboxes");
    },
})
export class Checkboxes extends NodeBlock {
    @definition
    @affects("#name")
    checkboxes = Collection.of(Checkbox, this as Checkboxes);

    @definition
    @affects("#required")
    required?: boolean;

    @definition
    @affects("#label")
    @affects("#collection", "checkboxes")
    alias?: string;

    @definition
    @affects("#collection", "checkboxes")
    exportable?: boolean;

    get label() {
        return npgettext(
            "block:checkboxes",
            "%2 (%1 checkbox)",
            "%2 (%1 checkboxes)",
            this.checkboxes.count,
            this.type.label
        );
    }

    @editor
    defineEditor(): void {
        this.editor.name();
        this.editor.description();
        this.editor.explanation();
        this.editor.collection({
            collection: this.checkboxes,
            title: pgettext("block:checkboxes", "Checkboxes"),
            placeholder: pgettext("block:checkboxes", "Unnamed checkbox"),
            editable: true,
            sorting: "manual",
        });

        this.editor.groups.options();
        this.editor.required(this);
        this.editor.visibility();
        this.editor.alias(this);
        this.editor.exportable(this);
    }

    @conditions
    defineConditions(): void {
        this.checkboxes.each((checkbox: Checkbox) => {
            if (checkbox.name) {
                this.conditions.template({
                    condition: CheckboxCondition,
                    label: checkbox.name,
                    icon: ICON_CHECKED,
                    props: {
                        slot: this.slots.select(checkbox.id),
                        checkbox: checkbox,
                        checked: true,
                    },
                });
            }
        });

        if (this.checkboxes.count > 0) {
            this.conditions.template({
                condition: UncheckedCondition,
            });
        }
    }
}
